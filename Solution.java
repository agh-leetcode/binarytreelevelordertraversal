/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */


class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        ArrayList<List<Integer>> arrays = new ArrayList();
        helper(root, arrays, 0);
        return arrays;
    }

    private void helper(TreeNode node, ArrayList<List<Integer>> arrays, int depth) {
        if (node==null)
            return;
        if (depth >= arrays.size()){
            arrays.add(new ArrayList<Integer>());
        }
        arrays.get(depth).add(node.val);
        helper(node.left,arrays,depth+1);
        helper(node.right,arrays,depth+1);
    }
}